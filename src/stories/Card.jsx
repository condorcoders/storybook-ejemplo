import React from 'react';
import './card.css';
import Picture from './assets/CondorCoders_SymbolNoBG_FullColour.png';

export const Card = () => (
    <div>
        <div className="card">
            <div className="card-avatar">
                <img src={Picture} />
            </div>
            <div className="card-details">
                <div className="name">Sofia Grijalva</div>
                <div className="occupation">Software Developer</div>

                <div className="card-about">
                    <div className="item">
                        <span className="value">27</span>
                        <span className="label">Años</span>
                    </div>
                    <div className="item">
                        <span className="value">175 cm</span>
                        <span className="label">Altura</span>
                    </div>
                </div>
                <div className="skills">
                    <span className="value">Programadora Front-end en ABC News. Creadora de Condor Coders.</span>
                </div>
            </div>
        </div>
        <p>Code Snippet de <a href="https://www.csscodelab.com/simple-html-css-business-cards-code-snippet/">CSS Code Lab</a></p>
    </div>
)
